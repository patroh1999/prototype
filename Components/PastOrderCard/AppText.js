//Author : Jay Patel
import React from "react";
import { Text, StyleSheet, Platform } from "react-native";

import styles from "./styles";

export default function AppText({ children, style, ...otherProps }) {
  return (
    <Text style={[styles.text, style]} {...otherProps}>
      {children}
    </Text>
  );
}
