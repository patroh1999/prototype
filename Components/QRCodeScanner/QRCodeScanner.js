//Author : Rohan Patel
import React, {useEffect, useState} from "react";
import {StyleSheet, Text, View} from "react-native";
import {BarCodeScanner} from "expo-barcode-scanner";
import LottieView from "lottie-react-native/src/js";

const QRCodeScanner = props => {
    const orderType = props.orderType;
    const [hasPermission, setHasPermission] = useState(null);
    useEffect(() => {
        (async () => {
            const {status} = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === "granted");
        })();
    }, []);

    const handleBarCodeScanned = ({type, data}) => {
        if (orderType === -1) {
            props.handleBarCodeScan(type, data);
        }
    };

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View
            style={{
                flex: 1,
                flexDirection: "column"
            }}
        >
            <BarCodeScanner
                onBarCodeScanned={handleBarCodeScanned}
                barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
                style={[StyleSheet.absoluteFillObject, {flex: 1, alignItems: "center"}]}
            >

                <LottieView
                    source={require("../../assets/scan.json")}
                    autoPlay
                    loop={true}
                    style={styles.lottieImage}
                />
                <Text style={{color: 'white', fontSize: 30, marginTop: '40%'}}>Scan the QR Code</Text>
            </BarCodeScanner>
        </View>
    );
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: "blue"
    },
    lottieImage: {
        height: 250,
        marginTop: '25%'
    }
});

export default QRCodeScanner;
