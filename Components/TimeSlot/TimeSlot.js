//Author : Rohan Patel
import React, {useEffect, useState} from "react";
import {View,TouchableOpacity,Text} from "react-native";
import axios from "axios";
import styles from "./TimeSlotStyle";
import baseURL from "../../dummyData/baseURL";

const TimeSlot = props=>{

    const timeSlot = props.timeSlot.item;
    const tConvert = time => {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1); // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original
        // string
    }
    return(
        <View style={[styles.mainContainer
            ,timeSlot.available ? styles.available : styles.notAvailable
        ,(timeSlot.selected && timeSlot.available) ? {backgroundColor:'orange'} : {}]}>
            <TouchableOpacity onPress={()=>props.onTimeSlotClickHandler(props.timeSlot.index)}>
                <Text style={styles.timeText}>{tConvert(timeSlot.time)}</Text>
            </TouchableOpacity>
        </View>
    );
};

export default TimeSlot


