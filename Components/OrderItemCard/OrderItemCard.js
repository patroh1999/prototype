//Author : Rohan Patel
import React from "react";
import {Image, SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import StarRating from "react-native-star-rating";
import styles from "./OrderItemCardStyle.js";
import {Ionicons} from "@expo/vector-icons";
import GestureRecognizer from "react-native-swipe-gestures";
import { useNavigation } from '@react-navigation/native';

const OrderItemCard = props => {
    const onDeleteItem = _=>{
        props.removeDish();
    };

    return (
        <GestureRecognizer config={{
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
        }}
                           onSwipeLeft={() => onDeleteItem()}
        >
            <SafeAreaView style={styles.menuCard}>
                <View style={{flex: 2, justifyContent: 'center'}}>
                    <Image source={{uri: `${props.dish.dish.image}`}}
                           style={styles.dishImage}
                    >

                    </Image>
                </View>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <Text style={styles.text}>{props.dish.quantity}x</Text>
                </View>
                <View style={{flex: 3, justifyContent: 'center'}}>
                    <Text style={styles.text}>{props.dish.dish.name}</Text>
                    {props.dish.note.trim() != "" && <Text style={styles.note}>"{props.dish.note}"</Text>}
                </View>
                {!props.delete && <View style={{flex: 2, justifyContent: 'center', alignItems: 'flex-end'}}>
                    <Text style={styles.priceText}>${props.dish.dish.price}</Text>
                </View>}
                {props.delete && <View style={{flex: 2, alignItems: "flex-end", justifyContent: 'center'}}
                                       onResponderRelease={() => {
                                       }}
                                       onStartShouldSetResponder={() => true}>
                    <Ionicons name={'md-trash'} size={24} color={'black'}/>
                </View>}

            </SafeAreaView>
        </GestureRecognizer>
    );
};
export default OrderItemCard;
