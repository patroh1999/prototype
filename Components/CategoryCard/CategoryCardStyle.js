//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    mainContainerSelected: {
        flex: 1,
        marginTop: 15,
        marginLeft: 10,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#FC5565',
        justifyContent: 'center'
    },
    mainContainer: {
        flex: 1,
        marginTop: 15,
        marginLeft: 10,
        height: 40,
        borderRadius: 25,
        justifyContent: 'center'
    },
    image: {
        flex: 1,
        flexDirection: 'row',
        resizeMode: "cover",
        justifyContent: "center",
        borderRadius: 15,
        overflow: "hidden",
        shadowColor: 'rgba(0, 0, 0, 0.7)',
        shadowOffset: {
            width: 0,
            height: 5
        },
        shadowRadius: 3,
        shadowOpacity: 1
    },
    nameSelected: {
        color: 'white',
        marginTop: 6,
        marginBottom: 6,
        marginLeft: 20,
        marginRight: 20,
        fontSize: 15,
        fontWeight: "400"
    },
    name: {
        marginTop: 6,
        marginBottom: 6,
        marginLeft: 20,
        marginRight: 20,
        fontSize: 16,
        fontWeight: "400"
    }
});
export default styles;
