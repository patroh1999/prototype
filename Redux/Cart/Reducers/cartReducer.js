//Author : Rohan Patel, Jay Patel
import {
  ADD_TO_CART,
  CLEAR_CART,
  REMOVE_DISH
} from "../Actions/cartAcionsTypes";

const initialState = {items:[],total:0};

const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART:
      const newState = {...state};
      newState.items.push(payload);
      newState.total = newState.total + payload.dish.price*payload.quantity;
      console.log(newState);
      return newState;

    case CLEAR_CART:
      return {items: [],total: 0};

      // Author : Jay Patel
    case REMOVE_DISH:
        console.log("REMOVE ID CALLED");
      const newRemoveDishState = {...state};
      for (let i = 0; i < newRemoveDishState.items.length; i++) {
        if (newRemoveDishState.items[i].id === payload) {
          newRemoveDishState.total -= newRemoveDishState.items[i].dish.price*newRemoveDishState.items[i].quantity;
          newRemoveDishState.items.splice(i, 1);
          return JSON.parse(JSON.stringify(newRemoveDishState));
        }
      }
      return state;

    default:
      return state;
  }
};

export default cartReducer;
