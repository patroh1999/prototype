//Author : Rohan Patel
import {ADD_ORDER_TYPE} from "../Actions/orderActionsTypes";

const initialState = {};

const orderReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ADD_ORDER_TYPE:
            return payload;
        default :
            return state;
    }
};

export default orderReducer;