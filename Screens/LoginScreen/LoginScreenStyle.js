//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#FEFEFE",
    },
    loginImage: {
        height: 380,
    },
    registerImage: {
        height: "100%",
    },
    loginSection: {
        flex: 3,
        width: "100%",
    },
    loginButton: {
        width: 250,
        borderColor: "#E9E8EB",
        borderWidth: 2,
        height: 50,
        borderRadius: 25,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
    },
    registerButton: {
        width: 250,
        backgroundColor: "#595FFF",
        height: 50,
        borderRadius: 25,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
        shadowColor: "rgba(0,0,0,.3)", // IOS
        shadowOffset: {height: 3, width: 2}, // IOS
        shadowOpacity: 4, // IOS
        shadowRadius: 1, //IOS
        elevation: 3, // Android
    },
    getStartedContainer: {
        height: 58,
        alignItems: "center",
        justifyContent: "center",
        width: "60%",
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 12},
        shadowOpacity: 0.1,
        shadowRadius: 48,
        elevation: 3,
    },
    gradientContainer: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 30,
    },
    getStartedTouchable: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    getStartedText: {
        color: "white",
        fontSize: 19,
        fontWeight: "bold",
    },
});
export default styles;
