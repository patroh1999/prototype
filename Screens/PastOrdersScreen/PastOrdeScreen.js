//Author : Jay Patel
import React, {useEffect, useState} from "react";
import {Text, View, SafeAreaView, FlatList} from "react-native";
import {connect} from "react-redux";
import styles from "./PastOrderScreenStyle";
import PastOrderCard from "../../Components/PastOrderCard/PastOrdeCart";
import axios from "axios";


import baseURL from "../../dummyData/baseURL";

const PastOrderScreen = props => {


    const [orders, setOrders] = useState([]);


    useEffect(_ => {
        axios.post(baseURL + "/order/" + props.userId).then(res => {
            console.log(res.data);
            setOrders(res.data);
        }).catch(err => {
            console.log(err)
        });

    }, []);
    return (
        <SafeAreaView style={styles.mainContainer}>

                    <FlatList data={orders} renderItem={({item}) => <PastOrderCard key={item.userId}
                                                                                   image={item.restaurant.bgImg}
                                                                                   title={item.restaurant.name}
                                                                                   subTitle={"$" + (item.total + item.tax).toFixed(2)}
                                                                                   totalItems={item.orderItems.length}
                                                                                   time={item.orderPlacedTime}
                    />}
                              style={styles.list}
                    />
        </SafeAreaView>
    )
};

const mapStateToProps = (state) => ({
    userId: state.user.id,
});
const mapActionsToProps = {};

export default connect(mapStateToProps, mapActionsToProps)(PastOrderScreen);