//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        backgroundColor: 'white'
    },
    list:{
        marginTop:50
    },
    detailContainer: {
        padding: 20,
        flex: 1,
        backgroundColor:'white'
    },
    image: {
        width: "100%",
        height: 300,
    },
    title: {
        fontSize: 18,
        fontWeight: "500",
    },
    price: {
        color: '#4ecdc4',
    },
    userContainer: {
        marginTop: 40,
    },
});
export default styles;
