//Author : Rohan Patel
import React from "react";
import {View, Text, SafeAreaView, TouchableOpacity} from "react-native";
import Styles from "./OnboardingStyle";
import LottieView from "lottie-react-native/src/js";
import {LinearGradient} from "expo-linear-gradient";

const OnboardingScreen = (props) => {
    return (
        <SafeAreaView style={Styles.mainContainer}>
            <View style={Styles.textContainer}>
                <Text style={Styles.textOne}>Welcome</Text>
                <Text style={Styles.textOne}>To</Text>
                <Text style={Styles.textTwo}>WayOrder</Text>
            </View>

            <View style={Styles.animationContainer}>
                <LottieView
                    source={require("../../assets/helloOnboarding.json")}
                    autoPlay
                    loop={false}
                    style={{width: "80%"}}
                />
            </View>

            <View style={Styles.buttonContainer}>
                <View style={Styles.getStartedContainer}>
                    <LinearGradient
                        colors={["#FE2382", "#FA8748"]}
                        style={Styles.gradientContainer}
                        start={[0.0, 0.5]}
                        end={[1.0, 0.5]}
                        locations={[0.0, 1.0]}
                    >
                        <TouchableOpacity
                            style={Styles.getStartedTouchable}
                            onPress={() => props.navigation.navigate("Register")}
                        >
                            <Text style={Styles.getStartedText}>GET STARTED</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

                <Text
                    style={{fontWeight: "600", marginTop: "3%"}}
                    onPress={() => props.navigation.navigate("Login")}
                >
                    Login
                </Text>
            </View>
        </SafeAreaView>
    );
};

export default OnboardingScreen;
