//Author : Rohan Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "white",
    },
    name: {
        fontSize: 28,
        fontWeight: "bold",
        marginTop: 6,
        letterSpacing: 0.3,
        marginLeft: 15
    },
    greeting: {
        fontSize: 18,
        fontWeight: "400",
        color: "#9AA0B0",
    },
    logoutButton: {
        flexDirection: "row",
        backgroundColor: "red",
        height: 50,
        marginLeft: "10%",
        marginRight: "10%",
        padding: 10,
        justifyContent: "center",
        borderRadius: 8,
    },
    qrButton: {
        flexDirection: "row",
        backgroundColor: "#595FFF",
        height: 53,
        marginLeft: "10%",
        marginRight: "10%",
        marginTop: "4%",
        padding: 10,
        justifyContent: "center",
        borderRadius: 8,
    },
    orderCard: {
        height: "15%",
        flexDirection: "row",
        backgroundColor: "#E1F2FF",
        marginLeft: "5%",
        marginRight: "5%",
        marginTop: "10%",
        borderRadius: 15,
    },
    orderText: {
        fontSize: 25,
        fontWeight: "bold",
        letterSpacing: 0.2,
    },
    tillDateText: {
        fontSize: 15,
        fontWeight: "300",
        marginTop: 10,
        marginLeft: 1,
        color: "#747F96",
    },
    searchBarContainer: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        borderBottomWidth: 0,
        paddingLeft: '4%',
        paddingRight: '4%'
    },
    searchBar: {
        backgroundColor: '#F1F4F9',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 10
    }
});
export default styles;
