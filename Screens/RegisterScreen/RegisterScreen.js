//Author : Rohan Patel
import React, {useState, useRef} from "react";
import {
    SafeAreaView,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Alert,
} from "react-native";
import {connect, Provider} from "react-redux";
import {Input} from "react-native-elements";
import styles from "./RegisterScreenStyle";
import {
    updateEmail,
    updateFullName,
    updatePassword,
} from "../../Redux/User/Actions/userActions";
import axios from "axios";
import baseURL from "../../dummyData/baseURL";
import {Base64} from "js-base64";
import {LinearGradient} from "expo-linear-gradient";
import {OutlinedTextField} from "react-native-material-textfield";
import * as EmailValidator from "email-validator";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import AwesomeAlert from "react-native-awesome-alerts";

const RegisterScreen = (props) => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [pass, updatePass] = useState("");
    const [pass2, updatePass2] = useState("");

    const [nameError, setNameError] = useState("");
    const [emailError, setEmailError] = useState("");
    const [mainPassError, setMainPassError] = useState("");
    const [passError, setPassError] = useState("");

    const [showAlert, setShowAlert] = useState(false);
    let fieldRef = React.createRef();
    const clearUserState = () => {
        props.onEmailUpdate("");
        props.onPasswordUpdate("");
    };

    const validateInputs = () => {
        let status = true;
        if (name.trim() == "") {
            setNameError("Name cannot be empty");
            status = false;
        }
        if (!EmailValidator.validate(email)) {
            setEmailError("Invalid Email");
            status = false;
        }
        if (email.trim() == "") {
            setEmailError("Email cannot be empty");
            status = false;
        }
        if (pass !== pass2) {
            setPassError("Password does not match");
            status = false;
        }
        if (pass.trim() == "") {
            setMainPassError("Password cannot be empty");
            status = false;
        }

        return status;
    };

    const onNameChangeHandler = (n) => {
        if (n.trim() != "") {
            setNameError("");
        } else {
            setNameError("Name cannot be empty");
        }
        setName(n);
    };
    const onEmailChangeHandler = (e) => {
        if (!EmailValidator.validate(e)) {
            setEmailError("Invalid email id");
        } else {
            setEmailError("");
        }
        setEmail(e);
    };

    const onConfirmPassChangeHandler = (cp) => {
        if (pass != cp) {
            setPassError("Password does not match");
        } else {
            setPassError("");
        }
        updatePass2(cp);
    };

    const onPassChangeHandler = (p) => {
        if (p.trim() != "") {
            setMainPassError("");
        } else {
            setMainPassError("Password cannot be empty");
        }
        updatePass(p);
    };
    const onRegisterPressHandler = () => {
        setShowAlert(true);
        if (validateInputs()) {
            props.onFullNameUpdate(name);
            props.onEmailUpdate(email);
            props.onPasswordUpdate(Base64.encode(pass));
            axios
                .put(baseURL + "/user/register", props.user)
                .then((res) => {
                    if (res.data === "") {
                        Alert.alert(
                            "Opps, Something went wrong",
                            "An account already exist with the provided email.",
                            [{text: "Try Again"}]
                        );
                    } else {
                        clearUserState();
                        Alert.alert(
                            "Registration successful",
                            "Please login with the account you created",
                            [
                                {
                                    text: "Login",
                                    onPress: () => props.navigation.navigate("Login"),
                                },
                            ]
                        );
                    }
                })
                .catch((err) => {
                    console.error(err);
                });
        }
    };
    return (
        <KeyboardAwareScrollView
            style={{backgroundColor: "white"}}
            resetScrollToCoords={{x: 0, y: 0}}
            contentContainerStyle={styles.mainContainer}
            scrollEnabled={false}
        >
            <SafeAreaView style={{flex: 2}}>
                <Image
                    source={require("../../assets/croods.png")}
                    style={styles.registerImage}
                    resizeMode={"contain"}
                />
            </SafeAreaView>

            <View style={styles.loginSection}>
                <Text
                    style={{
                        fontSize: 40,
                        fontWeight: "bold",
                        marginTop: 25,
                        marginLeft: 25,
                    }}
                >
                    Sign Up!
                </Text>
                <View
                    style={{flex: 1, justifyContent: "center", alignItems: "center"}}
                >
                    <OutlinedTextField
                        label="Name"
                        containerStyle={{width: 320, marginTop: 15}}
                        keyboardType={"default"}
                        error={nameError}
                        onChangeText={(text) => onNameChangeHandler(text)}
                        ref={fieldRef.current}
                        autofocus={true}
                    />
                    <OutlinedTextField
                        label="Email"
                        containerStyle={{width: 320, marginTop: 15}}
                        error={emailError}
                        onChangeText={(text) => onEmailChangeHandler(text)}
                        ref={fieldRef.current}
                    />
                    <OutlinedTextField
                        label="Password"
                        containerStyle={{width: 320, marginTop: 15}}
                        onChangeText={(text) => onPassChangeHandler(text)}
                        error={mainPassError}
                        secureTextEntry={true}
                    />
                    <OutlinedTextField
                        label="Confirm Password"
                        containerStyle={{width: 320, marginTop: 15}}
                        error={passError}
                        onChangeText={(text) => onConfirmPassChangeHandler(text)}
                        secureTextEntry={true}
                    />
                </View>
            </View>
            <View style={{flex: 1, alignItems: "center", width: "100%"}}>
                <View style={styles.getStartedContainer}>
                    <LinearGradient
                        colors={["#FE2382", "#FA8748"]}
                        style={styles.gradientContainer}
                        start={[0.0, 0.5]}
                        end={[1.0, 0.5]}
                        locations={[0.0, 1.0]}
                    >
                        <TouchableOpacity
                            style={styles.getStartedTouchable}
                            onPress={() => onRegisterPressHandler()}
                        >
                            <Text style={styles.getStartedText}>SIGN UP</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            </View>
        </KeyboardAwareScrollView>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
};

const mapActionsToProps = {
    onFullNameUpdate: updateFullName,
    onEmailUpdate: updateEmail,
    onPasswordUpdate: updatePassword,
};
export default connect(mapStateToProps, mapActionsToProps)(RegisterScreen);
