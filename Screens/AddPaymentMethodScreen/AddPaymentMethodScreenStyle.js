//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    addCardButtonContainer: {
        height: 58,
        alignItems: "center",
        justifyContent: "center",
        width: "60%",
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 12},
        shadowOpacity: 0.1,
        shadowRadius: 48,
        elevation: 3,
    },
    gradientContainer: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 25,
    },
    addCardTouchable: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    addCardText: {
        color: "white",
        fontSize: 19,
        fontWeight: "bold",
    }
});
export default styles;
