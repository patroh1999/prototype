//Author : Rohan Patel
import React, {useState} from "react";
import {FlatList, SafeAreaView, Text, View, TouchableOpacity} from "react-native";
import styles from "./CheckoutScreenStyle";
import PaymentMethodCard from "../../Components/PaymentMethodCard/PaymentMethodCard";
import GradientButton from "../../Components/GradientButton/GradientButton";
import axios from "axios";
import baseUrl from "../../dummyData/baseURL";
import {clearCart, removeDish} from "../../Redux/Cart/Actions/cartActions";
import {addOrder} from "../../Redux/User/Actions/userActions";
import {connect} from "react-redux";


const CheckoutScreen = props => {

    const [paymentMethods, setPaymentMethods] = useState([
        {id: 1, number: '**** **** **** 1234', selected: true},
        {id: 2, number: '**** **** **** 1235', selected: false},
        {id: 3, number: '**** **** **** 1236', selected: false},
        {id: 4, number: '**** **** **** 1237', selected: false},
        {id: 5, number: '**** **** **** 1237', selected: false},
        {id: 6, number: '**** **** **** 1237', selected: false}
    ]);

    const {placeOrderHandler} = props.route.params
    const onSelectCardHandler = id => {
        let paymentMethodCpy = [...paymentMethods];
        for (let i = 0; i < paymentMethodCpy.length; i++) {
            if (paymentMethodCpy[i].id == id)
                paymentMethodCpy[i].selected = true;
            else
                paymentMethodCpy[i].selected = false;

        }
        console.log(paymentMethodCpy);
        setPaymentMethods(paymentMethodCpy);
    };

    // const placeOrderHandler = () => {
    //     let orderItems = [];
    //     for (const dish of props.route.params.data) {
    //         let newObj = {
    //             ...dish,
    //         };
    //         delete newObj["id"];
    //         orderItems.push(newObj);
    //     }
    //     axios
    //         .post(
    //             baseUrl + "/order/" + props.restaurant.id + "/" + props.user.id,
    //             orderItems
    //         )
    //         .then((res) => {
    //             alert("Order Placed Successfully");
    //             // props.onAddOrderCreated(res.data);
    //             props.onClearCart();
    //             console.log(props.dishes);
    //             props.navigation.navigate("MenuScreen");
    //         })
    //         .catch((err) => {
    //             console.log(err);
    //         });
    // };


    return (
        <SafeAreaView style={styles.mainContainer}>
            <View style={styles.v1}>
                <Text style={styles.selectText}>Select a payment method</Text>

                <FlatList
                    data={paymentMethods}
                    renderItem={({item}) => <PaymentMethodCard action={"select"} selected={item.selected}
                                                               number={item.number}
                                                               onPress={() => onSelectCardHandler(item.id)}/>}
                    keyExtractor={item => item.id.toString()}
                    style={{flex: 1, marginTop: 35}}
                />

                <TouchableOpacity onPress={() => props.navigation.navigate("AddNewPaymentMethod")}
                                  style={styles.addNewPaymentMethodTouchable}>
                    <Text style={styles.addNewPaymentMethodText}>+ Add a new payment method</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.v2}>
                <GradientButton text={"PAY\t$" + props.route.params.amount} onPressHandler={() => placeOrderHandler(props.table)}/>
            </View>
        </SafeAreaView>
    );
};

const mapStateToProps = (state) => ({
    dishes: state.cart.items,
    subTotal: state.cart.total,
    restaurant: state.restaurant,
    user: state.user,
    table:state.restaurant.table
});
const mapActionsToProps = {
    onClearCart: clearCart,
    onRemoveDish: removeDish,
    onAddOrderCreated: addOrder,
};

export default connect(mapStateToProps, mapActionsToProps)(CheckoutScreen);