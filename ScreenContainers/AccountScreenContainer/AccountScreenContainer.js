//Author : Rohan Patel
import React, {useEffect, useState} from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import SettingScreen from "../../Screens/AccountScreen/AccountScreen";

import SavedPaymentScreen from "../../Screens/SavedPaymentScreen/SavedPaymentScreen";
import AddPaymentMethod from "../../Screens/AddPaymentMethodScreen/AddPaymentMethodScreen";
import PastOrderScreen from "../../Screens/PastOrdersScreen/PastOrdeScreen";


const AccountScreenContainer = props => {

    const Setting = createStackNavigator();

    return (
        <Setting.Navigator
            screenOptions={{
                headerShown: false
            }}>
            <Setting.Screen name="Account" component={SettingScreen}
                            options={{
                                headerShown: true, headerBackTitle: '',
                                headerTitle: 'My Account', headerTransparent: true,
                                headerTintColor: 'black'
                            }}/>
            <Setting.Screen name="SavedPaymentMethods" component={SavedPaymentScreen}
                            options={{
                                headerShown: true, headerBackTitle: 'My Account',
                                headerTitle: '', headerTransparent: true,
                                headerTintColor: 'black'
                            }}/>
            <Setting.Screen name="AddNewPaymentMethod" component={AddPaymentMethod}
                            options={{
                                headerShown: true, headerBackTitle: 'Saved Cards',
                                headerTitle: '', headerTransparent: true,
                                headerTintColor: 'black'
                            }}/>
            <Setting.Screen name="PastOrders" component={PastOrderScreen}
                            options={{
                                headerShown: true, headerBackTitle: 'My Account',
                                headerTitle: 'Orders', headerTransparent: true,
                                headerTintColor: 'black'
                            }}/>
        </Setting.Navigator>
    );
}

export default AccountScreenContainer;